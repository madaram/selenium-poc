# Selenium-poc #

Small project to illustrate the use of selenium for tests.

### Compilation and installation ###

* tested with Java 8 (should work with old Java 6/7) and maven 2
* Firefox is required
* Simple mvn clean install, tested on ubuntu with firefox. You may want to skip the tests.

### Content ###

* Three simple progressive tests : fr.madaram.seleniumshow.firsttests
* basic use of selenium driver : fr.madaram.seleniumshow.firsttests.GoogleSearchFirstExampleTest
* Page pattern illustration : fr.madaram.seleniumshow.pages.google.GoogleSearchPage
* Integrate selenium with spring  
* A more complex example with iframe and new window : fr.madaram.seleniumshow.traps.HuglyNewWindowTest
