package fr.madaram.seleniumshow.firsttests;

import java.io.IOException;
import java.math.BigInteger;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.PageFactory;

import fr.madaram.seleniumshow.pages.google.GoogleSearchPage;
import fr.madaram.seleniumshow.utilities.ScreenShotHelper;

/**
 * Simple test to illustrate the page pattern with GoogleSearchPage class.
 *
 */
public class GoogleSearchWithPagePatternTest {

	private WebDriver driver;

	private GoogleSearchPage googlePage;

	/**
	 * Close the browser.
	 */
	@After
	public void closeBrowser() {
		driver.quit();
	}

	/**
	 *
	 * @throws IOException
	 *             if problem while taking screenshots.
	 */
	@Test
	public void googleFindResultForJavaTest() throws IOException {
		googlePage.goTo();
		Assert.assertEquals(
				"The page title should equal Google at the start of the test.",
				"Google", driver.getTitle());

		googlePage.takeScreenShot("screenshots/step0.png").searchFor("java")
				.takeScreenShot("screenshots/step1.png").waitForResults()
				.takeScreenShot("screenshots/step2.png");

		Assert.assertEquals(
				"Google should find at least one result for java query.", 1,
				googlePage.getNumberOfResults().compareTo(BigInteger.ZERO));

	}

	/**
	 * Open browser and set page element.
	 */
	@Before
	public void openBrowser() {
		driver = new FirefoxDriver();
		googlePage = new GoogleSearchPage(driver, new ScreenShotHelper(driver));
		PageFactory.initElements(driver, googlePage);
	}
}
