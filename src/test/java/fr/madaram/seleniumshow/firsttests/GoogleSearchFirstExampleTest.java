package fr.madaram.seleniumshow.firsttests;

import java.io.IOException;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import fr.madaram.seleniumshow.utilities.ScreenShotHelper;

/**
 * Simplest test to illustrate the use of selenium. In further tests, driver
 * should not be used !
 */
public class GoogleSearchFirstExampleTest {

	/** Wait timeout for selenium. */
	private static final int DEFAULT_WAIT_TIMEOUT = 10;

	private WebDriver driver;

	private ScreenShotHelper screenshotHelper;

	/**
	 * Close browser after each test.
	 */
	@After
	public void closeBrowser() {
		driver.quit();
	}

	/**
	 * Goes to google, search java, wait for results then take picture.
	 * @throws IOException
	 *             if problem while saving picture
	 */
	@Test
	public void googleFindResultForJavaTest() throws IOException {
		Assert.assertEquals(
				"The page title should equal Google at the start of the test.",
				"Google", driver.getTitle());
		final WebElement searchField = driver.findElement(By.id("lst-ib"));
		searchField.sendKeys("java");
		final WebElement searchButton = driver.findElement(By.name("btnG"));
		searchField.submit();
		searchButton.click();
		new WebDriverWait(driver, DEFAULT_WAIT_TIMEOUT)
				.until(ExpectedConditions.presenceOfElementLocated(By
						.id("resultStats")));

		screenshotHelper.saveScreenshot("screenshots/step1.png");

	}

	/**
	 * Open browser and goes to google.
	 */
	@Before
	public void openBrowser() {
		driver = new FirefoxDriver();
		driver.get("http://www.google.com");
		screenshotHelper = new ScreenShotHelper(driver);
	}

}
