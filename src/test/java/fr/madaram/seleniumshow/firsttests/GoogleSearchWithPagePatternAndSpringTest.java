package fr.madaram.seleniumshow.firsttests;

import java.io.IOException;
import java.math.BigInteger;

import javax.inject.Inject;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.context.annotation.Profile;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.madaram.seleniumshow.AbstractSeleniumTest;
import fr.madaram.seleniumshow.SpringConfiguration;
import fr.madaram.seleniumshow.pages.google.GoogleSearchPage;

/**
 * Test class to illustrate a way to integrate Spring with selenium. Note that
 * the selenium driver is now invisible.
 *
 */
@Profile({ "default", "firefox" })
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = SpringConfiguration.class)
public class GoogleSearchWithPagePatternAndSpringTest extends
		AbstractSeleniumTest {

	@Inject
	private GoogleSearchPage googlePage;

	/**
	 * Goes to google, search java, check if more than one result.
	 * @throws IOException
	 *             if problem while saving screenshot
	 */
	@Test
	public void googleFindResultForJavaTest() throws IOException {
		googlePage.goTo();
		Assert.assertEquals(
				"The page title should equal Google at the start of the test.",
				"Google", googlePage.getTitle());

		googlePage.searchFor("java").takeScreenShot("screenshots/step1.png")
				.waitForResults().takeScreenShot("screenshots/step2.png");

		Assert.assertEquals(
				"Google should find at least one result for java query.", 1,
				googlePage.getNumberOfResults().compareTo(BigInteger.ZERO));

	}

}
