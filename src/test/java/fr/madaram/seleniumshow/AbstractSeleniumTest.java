package fr.madaram.seleniumshow;

import javax.inject.Inject;

import org.junit.After;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.SessionNotFoundException;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Abstract class for selenium test with spring. Inject driver and close it at
 * the end of each test. Have dirty context for cleaning driver bean.
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = SpringConfiguration.class)
@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
public abstract class AbstractSeleniumTest {

	@Inject
	private WebDriver driver;

	/** Close browser if possible. */
	@After
	public void closeBrowser() {
		try {
			driver.quit();
		} catch (final SessionNotFoundException e) {
			// driver is already closed, just swallow the exception !
		}
	}

}
