package fr.madaram.seleniumshow.traps;

import java.io.IOException;

import javax.inject.Inject;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.context.annotation.Profile;

import fr.madaram.seleniumshow.AbstractSeleniumTest;
import fr.madaram.seleniumshow.pages.w3c.WschoolsHomePage;
import fr.madaram.seleniumshow.pages.w3c.WschoolsHuglyNewWindow;

/**
 * Advanced test to illustrate how to navigate through iframe and new window.
 * Maybe an iframe should be represented by a new class that follow page
 * pattern.
 */
@Profile({ "default", "firefox" })
public class HuglyNewWindowTest extends AbstractSeleniumTest {

	@Inject
	private WschoolsHuglyNewWindow wschoolsHuglyNewWindow;

	/**
	 * @throws IOException
	 *             if problem while saving screenshot
	 */
	@Test
	public void canNavigateThrougIframesAndNewWindows() throws IOException {
		wschoolsHuglyNewWindow.goTo().takeScreenShot("step0.png");

		final WschoolsHomePage homePage =
				wschoolsHuglyNewWindow.clickTryItAndSwitch();
		homePage.setFullScreen().takeScreenShot("step1.png");

		Assert.assertTrue(homePage.hasMenuReferences());

	}

	/**
	 * @throws IOException
	 *             if problem while saving screenshot
	 */
	@Test
	public void secondSameTest() throws IOException {
		wschoolsHuglyNewWindow.goTo();

		final WschoolsHomePage homePage =
				wschoolsHuglyNewWindow.clickTryItAndSwitch();
		homePage.setFullScreen();

		Assert.assertTrue(homePage.hasMenuReferences());

	}
}
