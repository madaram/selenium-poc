package fr.madaram.seleniumshow.pages.w3c;

import javax.inject.Inject;
import javax.inject.Named;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import fr.madaram.seleniumshow.pages.AbstractPage;
import fr.madaram.seleniumshow.utilities.ScreenShotHelper;

/**
 * Page element that represent http://www.w3schools.com/.
 */
@Named
public class WschoolsHomePage extends AbstractPage<WschoolsHomePage> {

	@FindBy(id = "menulinkreferences")
	private WebElement menuLinkreferences;

	/**
	 * @param driver
	 *            selenium web driver.
	 * @param screenshotHelper
	 *            utility class for managing screenshots.
	 */
	@Inject
	public WschoolsHomePage(final WebDriver driver,
			final ScreenShotHelper screenshotHelper) {
		super(driver, screenshotHelper);
	}

	/**
	 * Goes to http://www.w3schools.com/.
	 * @return current WThreeCHomepage instance
	 */
	public WschoolsHomePage goTo() {
		driver.get("http://www.w3schools.com/");
		return this;
	}

	/**
	 * @return true if element with id menulinkreferences is displayed.
	 */
	public boolean hasMenuReferences() {
		return menuLinkreferences.isDisplayed();
	}
}
