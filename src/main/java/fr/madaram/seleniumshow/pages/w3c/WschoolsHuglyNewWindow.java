package fr.madaram.seleniumshow.pages.w3c;

import javax.inject.Inject;
import javax.inject.Named;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import fr.madaram.seleniumshow.pages.AbstractPage;
import fr.madaram.seleniumshow.utilities.ScreenShotHelper;

/**
 * Class that represent
 * http://www.w3schools.com/jsref/tryit.asp?filename=tryjsref_win_open5. Maybe
 * the iframe should be represented by other classes.
 *
 */
@Named
public class WschoolsHuglyNewWindow extends
AbstractPage<WschoolsHuglyNewWindow> {

	private static final String TRY_IT_XPATH = "/html/body/button";

	// create Page that represent the iframe ?
	@FindBy(id = "iframeResult")
	private WebElement iframeResult;

	@FindBy(xpath = TRY_IT_XPATH)
	private WebElement tryIt;

	@Inject
	private WschoolsHomePage wThreeCHomepage;

	/**
	 * @param driver
	 *            selenium web driver
	 * @param screenshotHelper
	 *            utility class for managing screenshots
	 */
	@Inject
	public WschoolsHuglyNewWindow(final WebDriver driver,
			final ScreenShotHelper screenshotHelper) {
		super(driver, screenshotHelper);
	}

	/**
	 * Click try it and return homepage element.
	 * @return w3c homepage page element
	 */
	public WschoolsHomePage clickTryItAndSwitch() {
		switchToResultIframe().clickTryIt();
		switchToLastOpenedWindow();
		return wThreeCHomepage;
	}

	/**
	 * Goes to
	 * http://www.w3schools.com/jsref/tryit.asp?filename=tryjsref_win_open5.
	 * @return current WschoolsHuglyNewWindow instance
	 */
	public WschoolsHuglyNewWindow goTo() {
		driver.get("http://www.w3schools.com/jsref/tryit.asp?filename=tryjsref_win_open5");
		return this;
	}

	// private WschoolsHuglyNewWindow waitForTryIt() {
	// WebElement myDynamicElement = (new WebDriverWait(driver,
	// DEFAULT_WAIT_TIME)).until(ExpectedConditions
	// .presenceOfElementLocated(By.xpath(TRY_IT_XPATH)));
	// return this;
	// }

	/**
	 * Click try it button.
	 * @return current WschoolsHuglyNewWindow instance
	 */
	private WschoolsHuglyNewWindow clickTryIt() {
		tryIt.click();
		return this;
	}

	/**
	 * Switch to the result iframe. Maybe should return a new object that
	 * represent the iframe.
	 * @return current WschoolsHuglyNewWindow instance
	 */
	private WschoolsHuglyNewWindow switchToResultIframe() {
		driver.switchTo().frame(iframeResult);
		return this;
	}

}
