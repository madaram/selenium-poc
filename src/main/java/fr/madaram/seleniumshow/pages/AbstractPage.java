/**
 *
 */
package fr.madaram.seleniumshow.pages;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.IOException;

import javax.inject.Inject;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

import fr.madaram.seleniumshow.utilities.ScreenShotHelper;

/**
 * Abstract class for classes that follow the page pattern.
 *
 * @param <T>
 *            the page class
 */
@SuppressWarnings("unchecked")
public abstract class AbstractPage<T> {

	/**
	 * Default wait time in seconds when waiting for object to appear or to be
	 * clickable.
	 */
	final static protected int DEFAULT_WAIT_TIME = 10;

	@Inject
	/** Selenium web driver. */
	final protected WebDriver driver;

	@Inject
	/** Helper for taking screenshots. */
	protected ScreenShotHelper screenshotHelper;

	/**
	 * @param driver
	 *            selenium web driver.
	 * @param screenshotHelper
	 *            utility class for managing screenshots.
	 */
	@Inject
	public AbstractPage(final WebDriver driver,
			final ScreenShotHelper screenshotHelper) {
		this.driver = driver;
		this.screenshotHelper = screenshotHelper;
		PageFactory.initElements(driver, this);
	}

	/**
	 * @return current page title
	 * @see org.openqa.selenium.WebDriver#getTitle()
	 */
	public String getTitle() {
		return driver.getTitle();
	}

	// public void printIframes(PrintStream printStream) {
	// final List<WebElement> iframes = driver.findElements(By
	// .tagName("iframe"));
	// for (WebElement iframe : iframes) {
	// iframe.printStream.println(iframe.getAttribute("xpath") + " : "
	// + iframe.toString());
	// }
	// }

	/**
	 * Set browser to fullscreen.
	 * @return accessor to current page pattern object
	 */
	public T setFullScreen() {
		final Dimension screenSize =
				Toolkit.getDefaultToolkit().getScreenSize();
		final double width = screenSize.getWidth();
		final double height = screenSize.getHeight();
		((JavascriptExecutor) driver).executeScript("window.resizeTo(" + width
				+ ", " + height + ");");
		return (T) this;
	}

	/**
	 * @param screenshotHelper
	 *            the screenshotHelper to set
	 */
	public void setScreenshotHelper(final ScreenShotHelper screenshotHelper) {
		this.screenshotHelper = screenshotHelper;
	}

	/**
	 * @see fr.madaram.seleniumshow.utilities.ScreenShotHelper.
	 *      saveScreenshot(String)
	 * @param screenShotName
	 *            screenshot name
	 * @return accessor to current page pattern object
	 * @throws IOException
	 *             if problem while saving picture
	 */
	public T takeScreenShot(final String screenShotName) throws IOException {
		screenshotHelper.saveScreenshot(screenShotName);
		return (T) this;
	}

	/**
	 * @see fr.madaram.seleniumshow.utilities.ScreenShotHelper.
	 *      saveScreenshot(String, long)
	 * @param screenShotName
	 *            screenshot name
	 * @param waitBefore
	 *            time to wait in seconds
	 * @return accessor to current page pattern object
	 * @throws IOException
	 *             if problem while saving picture
	 */
	public T takeScreenShot(final String screenShotName, final long waitBefore)
			throws IOException {
		screenshotHelper.saveScreenshot(screenShotName, waitBefore);
		return (T) this;
	}

	/**
	 * Switch driver to last opened window.
	 */
	protected void switchToLastOpenedWindow() {
		for (final String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
	}

}
