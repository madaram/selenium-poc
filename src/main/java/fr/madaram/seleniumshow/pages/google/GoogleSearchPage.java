/**
 *
 */
package fr.madaram.seleniumshow.pages.google;

import java.math.BigInteger;

import javax.inject.Inject;
import javax.inject.Named;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import fr.madaram.seleniumshow.pages.AbstractPage;
import fr.madaram.seleniumshow.utilities.ScreenShotHelper;

/**
 * Page pattern illustration with google search page. The @FindBy attributes are
 * managed by selenium if PageFactory.initElements(driver, googlePage) is
 * invoked.
 */
@Named
public class GoogleSearchPage extends AbstractPage<GoogleSearchPage> {

	/**
	 * id of the element Environ X résultats (X secondes).
	 */
	private static final String RESULT_STATS_ID = "resultStats";

	private static final String WHITE_SPACE = " ";

	/** Environ X résultats (X secondes) element. */
	@FindBy(id = RESULT_STATS_ID)
	private WebElement resultStats;

	/** Search input bar. */
	@FindBy(id = "lst-ib")
	private WebElement searchBox;

	/** Search button. */
	@FindBy(name = "btnG")
	private WebElement searchButton;

	/**
	 * Represent google.com homepage / search page.
	 *
	 * @param driver
	 *            selenium driver
	 * @param screenShotHelper
	 *            helper for screenshots
	 */
	@Inject
	public GoogleSearchPage(final WebDriver driver,
			final ScreenShotHelper screenShotHelper) {
		super(driver, screenShotHelper);
	}

	/**
	 * Parse Environ X résultats (X secondes) element to get the number of
	 * results available.
	 *
	 * @return number of results found by google
	 */
	public BigInteger getNumberOfResults() {
		String str = resultStats.getText().replaceAll(WHITE_SPACE, "");
		str = str.replaceAll("[^0-9]+", WHITE_SPACE);
		return new BigInteger(str.trim().split(WHITE_SPACE)[0]);
	}

	/**
	 * Go to http://www.google.com.
	 *
	 * @return reference to current GoogleSearchPage object
	 */
	public GoogleSearchPage goTo() {
		driver.get("http://www.google.com");
		return this;
	}

	/**
	 * @param text
	 *            text to search with google
	 * @return reference to current GoogleSearchPage object
	 */
	public GoogleSearchPage searchFor(final String text) {
		searchBox.sendKeys(text);
		searchBox.submit();
		searchButton.click();
		return this;
	}

	/**
	 * Ask the driver to wait for google result.
	 *
	 * @return reference to current GoogleSearchPage object
	 */
	public GoogleSearchPage waitForResults() {
		new WebDriverWait(driver, DEFAULT_WAIT_TIME).until(ExpectedConditions
				.presenceOfElementLocated(By.id(RESULT_STATS_ID)));
		return this;
	}

}
