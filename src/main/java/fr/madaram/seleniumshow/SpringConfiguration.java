package fr.madaram.seleniumshow;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

/**
 * Spring configuration. Declare browser bean and scan project.
 *
 */
@Configuration
@ComponentScan("fr.madaram.seleniumshow")
public class SpringConfiguration {

	/**
	 * Hail to the true king of browsers.
	 * @return open source and universal browser.
	 */
	@Bean
	@Profile({ "default", "firefox" })
	public WebDriver firefoxDriver() {
		return new FirefoxDriver();
	}

}
