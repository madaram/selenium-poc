/**
 *
 */
package fr.madaram.seleniumshow.utilities;

import java.io.File;
import java.io.IOException;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

/** Utility class to manage pictures. */
@Named
public class ScreenShotHelper {

	/** Used to convert milliseconds in seconds. */
	private static final int MILLISECONDS_IN_ONE_SECOND = 1000;

	/** Add file extension if user does not write it. */
	private static final String PICTURE_FILE_EXTENSION = ".png";

	/** Screenshots directory. */
	private static final String SCREENSHOTS_DIRECTORY = "screenshots/";

	@Inject
	/** Selenium web driver. */
	private final WebDriver driver;

	/**
	 * @param driver
	 *            selenium driver
	 */
	@Inject
	public ScreenShotHelper(final WebDriver driver) {
		super();
		this.driver = driver;
	}

	/**
	 * @param screenshotFileName
	 *            name of screen shot, picture while be save at
	 *            /screenshots/{screenshotFileName}[.png] where [.png] is added
	 *            only if missing in screenshotFileName.
	 * @throws IOException
	 *             if problem while saving screenshot
	 */
	public void saveScreenshot(String screenshotFileName) throws IOException {
		if (!screenshotFileName.endsWith(PICTURE_FILE_EXTENSION)) {
			screenshotFileName = screenshotFileName + PICTURE_FILE_EXTENSION;
		}
		final File screenshot =
				((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(screenshot, new File(SCREENSHOTS_DIRECTORY
				+ screenshotFileName));
	}

	/**
	 * @param screenshotFileName
	 *            name of screen shot, picture while be save at
	 *            /screenshots/{screenshotFileName}[.png] where [.png] is added
	 *            only if missing in screenshotFileName. * @param waitBefore
	 *            time to wait before taking screenshot in seconds
	 * @throws IOException
	 *             if problem while saving screenshot
	 */
	public void saveScreenshot(final String screenshotFileName,
			final long waitBefore) throws IOException {
		try {
			Thread.sleep(waitBefore * MILLISECONDS_IN_ONE_SECOND);
		} catch (final InterruptedException e) {
			throw new RuntimeException("Error while waiting before screenshot",
					e);
		}
		this.saveScreenshot(screenshotFileName);

	}
}
